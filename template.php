<?php

/**
 * @file
 * This file provides theme override functions for the myhfcc theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function myhfcc_preprocess_html(&$variables) {

  // Add IE conditional CSS for homepage.
  if ($variables['is_front']) {
    drupal_add_css(path_to_theme() . '/css/ie9-front.css', array(
      'group' => CSS_THEME,
      'weight' => 115,
      'browsers' => array('IE' => 'lte IE 9', '!IE' => FALSE),
      'preprocess' => FALSE,
    ));
  }

}

/**
 * Implements template_preprocess_comment()
 */
function myhfcc_preprocess_comment(&$vars) {
  // Change the Permalink to display #1 instead of 'Permalink'
  $comment = $vars['comment'];
  $uri = entity_uri('comment', $comment);
  $uri['options'] += array('attributes' => array(
    'class' => 'permalink',
    'rel' => 'bookmark',
  ));
  $vars['permalink'] = l('#' . $vars['id'], $uri['path'], $uri['options']);
}
?>
